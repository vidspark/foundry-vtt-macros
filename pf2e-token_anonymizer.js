(async () => {
    const token = canvas.tokens._hover
    if (token === null){
        return ui.notifications.error("Please hover over a token")
    } else if(!token.actor){
        return ui.notifications.error("Cannot find an actor for this token!")
    } else if(token.actor.hasPlayerOwner){
        return ui.notifications.error("Please pick an NPC token")
    }
    
    preferred_traits = [
        'humanoid', 'fiend', 'fey', 'beast', 'construct', 'dragon', 'undead',
        'aberration', 'elemental', 'animal', 'celestial'
        ]
    available_traits = preferred_traits.filter(i => token.actor.traits.has(i))
    creature_type = available_traits[0] || 'creature'
    
    names = game.user.getFlag('world', 'names')
    if(names === undefined){
        game.user.setFlag('world','names',{[token.id]: token.data.name})
    } else{
        if(token.id in names){
            name = names[token.id]
            game.user.unsetFlag('world','names.' + token.id)
            await token.document.update({name : name})
            await token.actor.update({name: name})
        } else{
            game.user.setFlag('world','names',{[token.id]: token.data.name})
            await token.document.update({name: "Unknown " + creature_type})
            await token.actor.update({name: "Unknown " + creature_type})
        }
    }
})()
